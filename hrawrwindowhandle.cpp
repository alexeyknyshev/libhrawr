#include "hrawrwindowhandle.h"

#include <xdo>

#include <hrawr.h>
#include <hrawrprivate.h>
#include <hrawrprocesshandle.h>

#include <hrawrmouse.h>
#include <hrawrkeyboard.h>

HRawrWindowHandle::HRawrWindowHandle(HRawrProcessHandle *process, unsigned int windowId)
    : mWindowId(windowId),
      mProcess(process)
{
    mMouse = new HRawrMouse(process->getHRawr(), windowId);
    mKeyboard = new HRawrKeyboard(process->getHRawr(), windowId);
}

void HRawrWindowHandle::makeCurrent(int makeCurrentModeMask)
{
    if (makeCurrentModeMask & MKM_Activate)
    {
        xdo_activate_window(mProcess->getHRawr()->_getHRawrPrivate()->xDo, (Window)mWindowId);
    }

    if (makeCurrentModeMask & MKM_Click)
    {
        xdo_click_window(mProcess->getHRawr()->_getHRawrPrivate()->xDo, (Window)mWindowId, 1);
    }

    if (makeCurrentModeMask & MKM_Raise)
    {
        xdo_raise_window(mProcess->getHRawr()->_getHRawrPrivate()->xDo, (Window)mWindowId);
    }
}

#!/usr/bin/python

import sys, getopt

def printHelp():
	print "usage: class_template.py CLASSNAME\n"
	print "\t-h, --help display this help and exit\n"

def main(argv):
	try:
		opts, args = getopt.getopt(argv, 'h')
	except:
		print str(err) #will print something like "option -a not recognized"
		printHelp()
		sys.exit(2)
	
	if not args:	
		printHelp()
		sys.exit(0)
	
	for opt, arg in opts:
		if opt == "-h":
			printHelp()
			sys.exit(0)
	
	className = args[0]
	headerName = className.lower() + ".h"
	sourceName = className.lower() + ".cpp"

	try:
		header = open(headerName, "w")
		try:
			header.write("#ifndef " + className.upper() + "_H\n")
			header.write("#define " + className.upper() + "_H\n")
			header.write("\n")
			header.write("class " + className + "\n")
			header.write("{\n")
			header.write("\n")
			header.write("};\n")
			header.write("\n")
			header.write("#endif // " + className.upper() + "_H\n")
		finally:
			header.close()
	except IOError:
		print "Cannot open " + headerName + " for writing"
		sys.exit(1)

	try:
		source = open(sourceName, "w")
		try:
			source.write("#include \"" + headerName + "\"\n")
		finally:
			source.close()
	except IOError:
		print "Cannot open " + className.lower() + ".cpp for writing"
		sys.exit(1)
	

if __name__ == "__main__":
	main(sys.argv[1:])

#include "hrawrprivate.h"

#include <hrawrprocesshandle.h>

HRawrPrivate::~HRawrPrivate()
{
    const auto end = attachedProcesses.end();
    for (auto it = attachedProcesses.begin(); it != end; ++it)
    {
        delete it->second;
    }
    attachedProcesses.clear();

    xdo_free(xDo);
}

HRawrProcessHandle *HRawrPrivate::getProcessByPid(int pid)
{
    auto it =  attachedProcesses.find(pid);
    if (it != attachedProcesses.end())
    {
        return it->second;
    }

    return nullptr;
}

class HRawrKeyboard
{
public:
    bool sendText(const std::string &text, unsigned int microsecondsDelay);

    bool sendKeySequence(const std::string &sequence, unsigned int microsecondsDelay);

    void sendKeySequenceDown(const std::string &sequence, unsigned int microsecondsDelay);
    void sendKeySequenceUp(const std::string &sequence, unsigned int microsecondsDelay); 
    
private:
    HRawrKeyboard();
};
#include "hrawr.h"

#include <hrawrprivate.h>

#include <dirent.h>
#include <sys/stat.h>

#include <sstream>
#include <fstream>

#include <thread>
#include <algorithm>
#include <chrono>
#include <cstring>
#include <xdo>

HRawr::HRawr()
    : d(nullptr)
{
    d = new HRawrPrivate;
}

HRawr::HRawr(const std::string &display)
    : d(nullptr)
{
    if (display.empty())
    {
        d = new HRawrPrivate;
    }
    else
    {
        d = new HRawrPrivate(display);
    }
}

HRawr::~HRawr()
{
    delete d;
    d = nullptr;
}

HRawrProcessHandle *HRawr::attachToProcessByWindowId(unsigned int winId)
{
    const int pid = getWindowPid(winId);

    HRawrProcessHandle *process = attachToProcessByPid(pid);

    if (!process)
    {
        return nullptr;
    }

    process->addWindowId(winId);
    return process;
}

HRawrProcessHandle *HRawr::attachToProcessByPid(int pid)
{
    auto windowIdSet = getWindowIdsByPid(pid);

    if (windowIdSet.empty() && !getPidExistence(pid))
    {
        return nullptr;
    }

    HRawrProcessHandle *process = nullptr;

    const auto it = d->attachedProcesses.find(pid);
    if (it != d->attachedProcesses.end())
    {
        process = it->second;
        if (process)
        {
            process->updateWindowHandles();
        }
    }
    else
    {
        const auto it = d->attachedProcesses.find(pid);
        if (it == d->attachedProcesses.end())
        {
            const std::string processName = getProcessNameByPid(pid);
            process = new HRawrProcessHandle(this, pid, processName);
            d->attachedProcesses.insert({ pid, process });
        }
        else
        {
            process = it->second;
        }
    }

    return process;
}

HRawr::HRawrProcessHandleSet HRawr::attachToProcessByName(const std::string &namePattern)
{
    HRawr::HRawrProcessHandleSet result;

    struct stat st;
    if (stat("/proc/version", &st) >= 0)
    {
        /// proc mounted

        DIR *dir = opendir("/proc");
        if (dir == NULL)
        {
            /// could not open /proc dir
            return result;
        }

        struct dirent *dirEntry = NULL;
        while ( (dirEntry = readdir(dir)) != NULL)
        {
            /// Drop non directory entries
            if (dirEntry->d_type != DT_DIR)
            {
                continue;
            }

            std::istringstream stream(dirEntry->d_name);

            int pid;
            stream >> pid;
            if (!stream)
            {
                /// couldn't convert to int (not a process proc entry)
                continue;
            }

            std::ifstream statFileStream;
            statFileStream.open(std::string("/proc/") + dirEntry->d_name + "/stat");
            if (!statFileStream.is_open())
            {
                /// couldn't open process's proc stat file
                continue;
            }

            statFileStream >> pid;

            std::string processName;
            statFileStream >> processName;
            _fixProcessName(processName);

            if (processName.find(namePattern) == std::string::npos)
            {
                /// no matches, continue searching
                continue;
            }

            HRawrProcessHandle *process = new HRawrProcessHandle(this, pid, processName);
            result.insert(process);
        }
    }

    return result;
}

/*
HRawrProcessHandle HRawr::attachToWindowCurrent()
{
    Window win;
    if (xdo_get_active_window(d->xDo, &win) == 0)
    {
        HRawrProcessHandle handle(this, win);
        d->attachedProcesses.insert(handle);
        return handle;
    }
    return HRawrProcessHandle();
}
*/

void HRawr::sleepForSeconds(unsigned int sec)
{
    std::this_thread::sleep_for(std::chrono::seconds(sec));
}

void HRawr::sleepForMilliSeconds(unsigned int ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

std::string HRawr::getWindowName(unsigned int windowId) const
{
    unsigned char *name = NULL;
    int nameLength = 0;
    int nameType = 0;

    if (xdo_get_window_name(d->xDo, (Window)windowId, &name, &nameLength, &nameType) == 0)
    {
        std::string result = (char *)name;
        XFree(name);
        return result;
    }

    return std::string();
}

int HRawr::getWindowPid(unsigned int windowId) const
{
    return xdo_get_pid_window(d->xDo, (Window)windowId);
}

std::set<unsigned int> HRawr::getWindowIdsByPid(int pid) const
{
    /// Prepare request

    xdo_search_t search;
    memset(&search, 0, sizeof(xdo_search_t));

    search.pid = pid;
    search.max_depth = -1;

    search.searchmask = SEARCH_PID;
    search.require = xdo_search::SEARCH_ALL;

    /// Search

    Window *windows = nullptr;
    unsigned int windowsCount = 0;

    xdo_search_windows(d->xDo, &search, &windows, &windowsCount);

    std::set<unsigned int> result;
    for (unsigned int i = 0; i < windowsCount; ++i)
    {
        result.insert(windows[i]);
    }

    XFree(windows);

    return result;
}

std::set<unsigned int> HRawr::getWindowIdsByName(const std::string &name, bool onlyVisible) const
{
    /// Prepare request

    xdo_search_t search;
    memset(&search, 0, sizeof(xdo_search_t));

    search.winname = name.c_str();

    search.only_visible = (int)onlyVisible;

    search.max_depth = -1; /// !!!!
    search.searchmask = SEARCH_NAME;
    search.require = xdo_search::SEARCH_ANY;

    /// Search

    Window *windows = nullptr;
    unsigned int windowsCount = 0;

    xdo_search_windows(d->xDo, &search, &windows, &windowsCount);

    std::set<unsigned int> result;
    for (unsigned int i = 0; i < windowsCount; ++i)
    {
        result.insert(windows[i]);
    }

    XFree(windows);

    return result;
}

std::string HRawr::getProcessNameByPid(int pid)
{
    if (!getPidExistence(pid))
    {
        return std::string();
    }

    const std::string pidStatPath = "/proc/" + std::to_string(pid) + "/stat";

    std::ifstream statFileStream;
    statFileStream.open(pidStatPath);
    if (statFileStream)
    {
        int pid;
        std::string processName;

        statFileStream >> pid;
        statFileStream >> processName;
        _fixProcessName(processName);
        return processName;
    }

    return "";
}

bool HRawr::getPidExistence(int pid)
{
    struct stat st;

    const std::string pidPath = "/proc/" + std::to_string(pid);

    if (stat(pidPath.c_str(), &st) == -1 && errno == ENOENT)
    {
        return false;
    }

    return true;
}

void HRawr::_fixProcessName(std::string &name)
{
    if (name.size() > 2)
    {
        if (name.at(0) == '(')
        {
            name.erase(0, 1);
        }

        if (name.at(name.size() - 1) == ')')
        {
            name.erase(name.size() - 1, 1);
        }
    }
}

#include "hrawrmouse.h"

#include <xdo>

#include <hrawr.h>
#include <hrawrprivate.h>

const int HRawrMouse::BUTTON_LEFT = 1;
const int HRawrMouse::BUTTON_RIGHT = 3;
const int HRawrMouse::BUTTON_MIDDLE = 2;

bool HRawrMouse::moveRelative(int x, int y)
{
    return (xdo_move_mouse_relative_to_window(mHRawr->d->xDo, (Window)mWindowId, x, y) == XDO_SUCCESS);
}

bool HRawrMouse::buttonClick(int button)
{
    if (button < BUTTON_LEFT || button > BUTTON_RIGHT)
    {
        /// TODO error;
        return false;
    }

    return (xdo_click_window(mHRawr->d->xDo, (Window)mWindowId, button) == XDO_SUCCESS);
}

bool HRawrMouse::buttonDown(int button)
{
    if (button < BUTTON_LEFT || button > BUTTON_RIGHT)
    {
        /// TODO error;
        return false;
    }

    return (xdo_mouse_down(mHRawr->d->xDo, (Window)mWindowId, button) == XDO_SUCCESS);
}

bool HRawrMouse::buttonUp(int button)
{
    if (button < BUTTON_LEFT || button > BUTTON_RIGHT)
    {
        /// TODO error;
        return false;
    }

    return (xdo_mouse_up(mHRawr->d->xDo, (Window)mWindowId, button) == XDO_SUCCESS);
}

bool HRawrMouse::wheelUp()
{
    return (xdo_mouse_up(mHRawr->d->xDo, (Window)mWindowId, 4) == XDO_SUCCESS);
}

bool HRawrMouse::wheelDown()
{
    return (xdo_mouse_down(mHRawr->d->xDo, (Window)mWindowId, 5) == XDO_SUCCESS);
}

HRawrPoint HRawrMouse::getScreenPosition(bool *ok) const
{
    HRawrPoint point;
    int retCode = xdo_get_mouse_location2(mHRawr->d->xDo, &point.x, &point.y, NULL, NULL);
    if (ok)
    {
        *ok = (retCode == XDO_SUCCESS);
    }

    return point;
}

bool HRawrMouse::setCentered()
{
    return setRelativeNormalizedPosition(0.5f, 0.5f);
}

bool HRawrMouse::setRelativeNormalizedPosition(float x, float y)
{
    unsigned int width = 0, height = 0;
    int retCode = xdo_get_window_size(mHRawr->d->xDo, (Window)mWindowId, &width, &height);
    if (retCode == XDO_SUCCESS && width != 0 && height != 0)
    {
        moveRelative(width * x, height * y);
        return true;
    }
    return false;
}

HRawrPoint HRawrMouse::getRelativePosition(bool *ok) const
{
    HRawrPoint result(0, 0);

    bool _ok = false;

    XWindowAttributes attr;
    int retCode = XGetWindowAttributes(mHRawr->d->xDo->xdpy, (Window)mWindowId, &attr);
    if (true)
    {
        int x = 0, y = 0;
        retCode = xdo_get_mouse_location(mHRawr->d->xDo, &x, &y, NULL);
        if (retCode == XDO_SUCCESS)
        {
            Window unusedChild;
            XTranslateCoordinates(mHRawr->d->xDo->xdpy,
                                  attr.root, (Window)mWindowId,
                                  x, y, &result.x, &result.y,
                                  &unusedChild);
            _ok = true;
        }
    }

    if (ok)
    {
        *ok = _ok;
    }

    return result;

}

HRawrPointF HRawrMouse::getRelativePositionNormalized(bool *ok) const
{
    HRawrPointF result(0.0f, 0.0f);

    bool _ok = false;
    HRawrPoint pos = getRelativePosition(&_ok);
    if (_ok)
    {
        unsigned int width = 0, height = 0;
        if (xdo_get_window_size(mHRawr->d->xDo, (Window)mWindowId,
                                &width, &height) == XDO_SUCCESS &&
            width != 0 && height != 0)
        {
            /// @remarks _ok is already true
            result.x = (float)pos.x / (float)width;
            result.y = (float)pos.y / (float)height;
        }
        else
        {
            _ok = false;
        }
    }

    if (ok)
    {
        *ok = _ok;
    }

    return result;
}

unsigned int HRawrMouse::getWindowUnderCursor() const
{
    Window windowId = 0;
    if (xdo_get_window_at_mouse(mHRawr->d->xDo, &windowId) == XDO_SUCCESS)
    {
        return (unsigned int)windowId;
    }

    return 0;
}

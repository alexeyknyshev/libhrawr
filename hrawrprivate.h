#ifndef HRAWRPRIVATE_H
#define HRAWRPRIVATE_H

#include <map>
#include <string>

#include <xdo>

class HRawrProcessHandle;

class HRawrPrivate
{
public:
    HRawrPrivate()
        : xDo(NULL)
    {
        xDo = xdo_new(NULL);
    }

    HRawrPrivate(const std::string &display)
        : xDo(nullptr)
    {
        xDo = xdo_new(display.c_str());
    }

    virtual ~HRawrPrivate();

    HRawrProcessHandle *getProcessByPid(int pid);

    xdo_t *xDo;
    std::map<int, HRawrProcessHandle *> attachedProcesses;
};

#endif // HRAWRPRIVATE_H

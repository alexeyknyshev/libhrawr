#ifndef HRAWR_H
#define HRAWR_H

#include <string>

#include <hrawrprocesshandle.h>

class HRawrPrivate;

class HRawr
{
    friend class HRawrProcessHandle;
    friend class HRawrWindowHandle;
    friend class HRawrMouse;
    friend class HRawrKeyboard;

public:
    HRawr();
    HRawr(const std::string &display);
    virtual ~HRawr();

    HRawrProcessHandle *attachToProcessByWindowId(unsigned int winId);
    HRawrProcessHandle *attachToProcessByPid(int pid);

    typedef std::set<HRawrProcessHandle *> HRawrProcessHandleSet;
    HRawrProcessHandleSet attachToProcessByName(const std::string &namePattern);

    void sleepForSeconds(unsigned int sec);
    void sleepForMilliSeconds(unsigned int milliSec);

    std::string getWindowName(unsigned int windowId) const;
    int getWindowPid(unsigned int windowId) const;

    std::set<unsigned int> getWindowIdsByPid(int pid) const;
    std::set<unsigned int> getWindowIdsByName(const std::string &name,
                                              bool onlyVisible) const;

    static std::string getProcessNameByPid(int pid);

    static bool getPidExistence(int pid);

protected:
    inline HRawrPrivate *_getHRawrPrivate() const
    { return d; }

private:
    HRawrPrivate *d;

    static void _fixProcessName(std::string &name);
};

#endif // HRAWR_H

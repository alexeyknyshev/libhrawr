#ifndef HRAWRPROCESSHANDLE_H
#define HRAWRPROCESSHANDLE_H

#include <string>
#include <set>
#include <map>

class HRawr;

class HRawrWindowHandle;

class HRawrProcessHandle
{
    friend class HRawr;

public:
    HRawrProcessHandle(HRawr *hRawr, int pid, const std::string &name);
    HRawrProcessHandle(const HRawrProcessHandle &other) = delete;
    HRawrProcessHandle(HRawrProcessHandle &&other) = delete;

    virtual ~HRawrProcessHandle();

    inline int getPid() const
    { return mPid; }

    bool sendSignal(int signal);

    inline void setName(const std::string &name)
    { mName = name; }

    inline const std::string &getName() const
    { return mName; }

    inline HRawr *getHRawr() const
    { return mHRawr; }

    typedef std::map<unsigned int, HRawrWindowHandle *> HRawrWindowHandleMap;

    const HRawrWindowHandleMap &getWindowHandles() const
    { return mWindowHandles; }

    HRawrWindowHandle *addWindowId(unsigned int winId);
    void updateWindowHandles();

    inline bool isValid() const
    { return mPid != 0; }

    inline bool operator==(const HRawrProcessHandle &other) const
    { return getPid() == other.getPid(); }

    inline bool operator!=(const HRawrProcessHandle &other) const
    { return getPid() != other.getPid(); }

    inline bool operator<(const HRawrProcessHandle &other) const
    { return getPid() < other.getPid(); }

private:
    HRawr *mHRawr;
    const int mPid;
    std::string mName;

    HRawrWindowHandleMap mWindowHandles;
};

#endif // HRAWRPROCESSHANDLE_H

#ifndef HRAWRWINDOWHANDLE_H
#define HRAWRWINDOWHANDLE_H

class HRawrProcessHandle;

class HRawrMouse;
class HRawrKeyboard;

class HRawrWindowHandle
{
public:
    HRawrWindowHandle(HRawrProcessHandle *process, unsigned int windowId);
    /// Input devices

    inline HRawrMouse *getMouse() const
    { return mMouse; }

    inline HRawrKeyboard *getKeyboard() const
    { return mKeyboard; }

    enum MakeCurrentMode
    {
        MKM_Activate = 0x1,
        MKM_Click = 0x2,
        MKM_Raise = 0x4,

        MKM_All = MKM_Activate | MKM_Click | MKM_Raise
    };

    void makeCurrent(int makeCurrentModeMask = MKM_Click | MKM_Raise);

private:
    const unsigned int mWindowId;
    const HRawrProcessHandle *mProcess;

    HRawrMouse *mMouse;
    HRawrKeyboard *mKeyboard;
};

#endif // HRAWRWINDOWHANDLE_H

#include "hrawrkeyboard.h"

#include <xdo>

#include <hrawr.h>
#include <hrawrprivate.h>

bool HRawrKeyboard::sendText(const std::string &text, useconds_t microsecondsDelay)
{
    return (xdo_enter_text_window(mHRawr->d->xDo, (Window)mWindowId,
                                  text.c_str(), microsecondsDelay) == XDO_SUCCESS);
}

bool HRawrKeyboard::sendKeySequence(const std::string &sequence, useconds_t microsecondsDelay)
{
    return (xdo_send_keysequence_window(mHRawr->d->xDo, (Window)mWindowId,
                                        sequence.c_str(), microsecondsDelay) == XDO_SUCCESS);
}

bool HRawrKeyboard::sendKeySequenceDown(const std::string &sequence, useconds_t microsecondsDelay)
{
    return (xdo_send_keysequence_window_down(mHRawr->d->xDo, (Window)mWindowId,
                                             sequence.c_str(), microsecondsDelay) == XDO_SUCCESS);
}

bool HRawrKeyboard::sendKeySequenceUp(const std::string &sequence, useconds_t microsecondsDelay)
{
    return (xdo_send_keysequence_window_up(mHRawr->d->xDo, (Window)mWindowId,
                                           sequence.c_str(), microsecondsDelay) == XDO_SUCCESS);
}

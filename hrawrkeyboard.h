#ifndef HRAWRKEYBOARD_H
#define HRAWRKEYBOARD_H

#include <string>

class HRawr;

class HRawrKeyboard
{
    friend class HRawrWindowHandle;

public:
    bool sendText(const std::string &text, useconds_t microsecondsDelay);

    bool sendKeySequence(const std::string &sequence, useconds_t microsecondsDelay);

    bool sendKeySequenceDown(const std::string &sequence, useconds_t microsecondsDelay);
    bool sendKeySequenceUp(const std::string &sequence, useconds_t microsecondsDelay);

private:
    HRawrKeyboard(HRawr *hRawr, unsigned int windowId)
        : mWindowId(windowId), mHRawr(hRawr)
    { }

    const unsigned int mWindowId;
    const HRawr *mHRawr;
};

#endif // HRAWRKEYBOARD_H

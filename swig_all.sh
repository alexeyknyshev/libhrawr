#!/bin/bash -e 

SELF=$(basename "$0")

echo "$SELF: --- libhrawr generating bindings (swig) ---"

sw=$(find /usr/bin -executable -type f -name "swig*")
echo "$SELF: swig excutable found: $sw"

command -v $sw >/dev/null 2>&1 ||
{
	echo "$SELF: swig required but not installed... Aborting"
	echo "$SELF: --- libhrawr generating bindings (swig) FAILED! ---"
	exit 1;			
}

fres=$(find /usr/share/swig* -name 'std_string.i' -type f -print 2>/dev/null | grep lua)

string_i=${fres[0]}
echo "$SELF: string template found: $string_i"

stdstring="%include <$string_i>"

find . -name '*.i' -type f -print 2>/dev/null |
while read path
do
	echo "$SELF: parsing swig interface file: $path"

	workpath=$path.work
	rm -f $workpath

	cp $path $workpath
	sed -i "s|@std_string|$stdstring|" $workpath

	dir=`dirname $path`
	bname=`basename $path`
	gen="$dir/${bname%.i}.cc"
	$sw -c++ -Wall -lua -o $gen $workpath
	echo "$SELF: cxx generated file: $gen"

	rm -f $workpath
done

echo "$SELF: --- libhrawr generating bindings (swig) completed ---"

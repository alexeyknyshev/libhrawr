class HRawrPoint
{
public:
    HRawrPoint();
    HRawrPoint(int x, int y);

    int x;
    int y;
};

class HRawrPointF
{
public:
    HRawrPointF();
    HRawrPointF(float x, float y);

    float x;
    float y;
};

class HRawrMouse
{
public:
    bool moveRelative(int x, int y);

    static const int BUTTON_LEFT;
    static const int BUTTON_RIGHT;
    static const int BUTTON_MIDDLE;

    bool buttonDown(int button);
    bool buttonUp(int button);

    bool wheelUp();
    bool wheelDown();

    bool setCentered();

    bool setRelativeNormalizedPosition(float x, float y);
    
private:
    HRawrMouse();
};
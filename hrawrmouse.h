#ifndef HRAWRMOUSE_H
#define HRAWRMOUSE_H

class HRawrPoint
{
public:
    HRawrPoint()
        : x(0), y(0)
    { }

    HRawrPoint(int x_, int y_)
        : x(x_), y(y_)
    { }

    int x, y;
};

class HRawrPointF
{
public:
    HRawrPointF()
        : x(0.0f), y(0.0f)
    { }

    HRawrPointF(float x_, float y_)
        : x(x_), y(y_)
    { }

    float x, y;
};

/// ------------------------------------

class HRawr;

class HRawrMouse
{
    friend class HRawrWindowHandle;

public:
    bool moveRelative(int x, int y);


    static const int BUTTON_LEFT;
    static const int BUTTON_RIGHT;
    static const int BUTTON_MIDDLE;

    bool buttonClick(int button);

    bool buttonDown(int button);
    bool buttonUp(int button);

    bool wheelUp();
    bool wheelDown();

    HRawrPoint getScreenPosition(bool *ok = nullptr) const;

    bool setCentered();

    bool setRelativeNormalizedPosition(float x, float y);
    HRawrPointF getRelativePositionNormalized(bool *ok = nullptr) const;

    HRawrPoint getRelativePosition(bool *ok = nullptr) const;

    unsigned int getWindowUnderCursor() const;

private:
    HRawrMouse(HRawr *hRawr, unsigned int windowId)
        : mWindowId(windowId), mHRawr(hRawr)
    { }

    const unsigned int mWindowId;
    const HRawr *mHRawr;
};

#endif // HRAWRMOUSE_H

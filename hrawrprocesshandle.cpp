#include "hrawrprocesshandle.h"

#include <hrawr.h>
#include <hrawrprivate.h>

#include <hrawrwindowhandle.h>

#include <hrawrmouse.h>
#include <hrawrkeyboard.h>

extern "C"
{
#include <signal.h>
}

HRawrProcessHandle::HRawrProcessHandle(HRawr *hRawr, int pid, const std::string &name)
    : mHRawr(hRawr),
      mPid(pid)
{
    setName(name);
    updateWindowHandles();
}

HRawrProcessHandle::~HRawrProcessHandle()
{
    const auto end = mWindowHandles.end();
    for (auto it = mWindowHandles.begin(); it != end; ++it)
    {
        delete it->second;
    }

    mWindowHandles.clear();
}

bool HRawrProcessHandle::sendSignal(int sig)
{
    const int pid = getPid();
    if (pid > 1)
    {
        if (kill(getPid(), sig) == 0)
        {
            return true;
        }

        /// TODO: check errno
    }
    else
    {
        /// TODO: case: Invalid pid
    }


    return false;
}

HRawrWindowHandle *HRawrProcessHandle::addWindowId(unsigned int winId)
{
    const auto it = mWindowHandles.find(winId);
    if (it == mWindowHandles.end())
    {
        return mWindowHandles.insert({ winId, new HRawrWindowHandle(this, winId) }).first->second;
    }
    return it->second;
}

void HRawrProcessHandle::updateWindowHandles()
{
    auto windowIdSet = mHRawr->getWindowIdsByPid(getPid());

    for (unsigned int winId : windowIdSet)
    {
        auto windowIt = mWindowHandles.find(winId);
        if (windowIt == mWindowHandles.end())
        {
            mWindowHandles.insert({ winId, new HRawrWindowHandle(this, winId) });
        }
    }
}

/*
std::map<unsigned int, std::string> HRawrProcessHandle::getWindowNames() const
{
    if (!isValid())
    {
        return std::map<unsigned int, std::string>();
    }

    std::map<unsigned int, std::string> result;

    for (unsigned int id : mWindowIds)
    {
        std::string name = mHRawr->getWindowName(id);
        result.insert({ id, name });
    }

    return result;
}
*/

/*
void HRawrProcessHandle::focusWindow()
{
    if (!mWindowIds.empty())
    {
        xdo_focus_window(mHRawr->d->xDo, *mWindowIds.begin());
    }
}
*/
